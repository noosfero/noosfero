# German translation of noosfero.
# Copyright (C) 2009-2013 Josef Spillner
# Copyright (C) 2009, 2011 Ronny Kursawe
# This file is distributed under the same license as the noosfero package.
# Josef Spillner <josef.spillner@tu-dresden.de>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.3~rc2-1-ga15645d\n"
"PO-Revision-Date: 2022-12-14 07:47+0000\n"
"Last-Translator: Max Fritz <oratoris.meto@gmail.com>\n"
"Language-Team: German <https://hosted.weblate.org/projects/noosfero/"
"plugin-google-cse/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15-dev\n"

#, fuzzy
msgid ""
"A plugin that uses the Google Custom Search as Noosfero general search "
"engine."
msgstr ""
"Ein Plugin, welches Google Custom Search nutzt, um die generelle Suche in "
"Noosfero zu realisieren."

msgid "Loading"
msgstr "Lade"
